#ifndef PNPOPENCV_H_
#define PNPOPENCV_H_


#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>


#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>


#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv/cvwimage.h>
#include <opencv/highgui.h>

#include <Eigen/Dense>

//#include <eigen3/Eigen/Dense>

#include <iostream>

#include <std_msgs/Int8.h>

#include <fstream>

#include <geometry_msgs/PoseStamped.h>

#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/video/tracking.hpp>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <nav_msgs/Odometry.h>


#define FRAMES_PER_SECOND 25.0
#define MEASUREMENT_NOISE 0.1
#define PROCESS_NOISE 1e-4

#define NUM_ITERATIONS 500
#define MIN_REPROJECTION_ERROR 10.0
#define REPROJECTION_ERROR_INCREASE_RATIO 0.1

#define MIN_GOOD_POSES 10
#define MAX_BAD_POSES 10
#define MAX_COVARIANCE 0.05



#define CANNY_THR 100
#define ROI_RESIZE_RATIO 2.0
#define MIN_CIRCULARITY 0.6
#define INTENSITY_THR 115



//using namespace std;

class PnpOpencv
{

public:

  typedef Eigen::Matrix<Eigen::Vector2d, Eigen::Dynamic, 1> List2DPoints; //!< A dynamic column vector containing Vector2D elements. \see Vector2d
  typedef Eigen::Matrix<Eigen::Vector3d, Eigen::Dynamic, 1> List3DPoints; //!< A dynamic column vector containing Vector3D elements. \see Vector3d
  typedef Eigen::Matrix<Eigen::Vector4d, Eigen::Dynamic, 1> List4DPoints; //!< A dynamic column vector containing Vector4D elements. \see Vector4d


public:

  PnpOpencv();
  ~PnpOpencv();

  cv::KalmanFilter KF;  // instantiate Kalman Filter

  int nStates;            // the number of states
  int nMeasurements;       // the number of measured states
  int nInputs;             // the number of control actions
  double dt;           // time between measurements (1/FPS)

  void initKalmanFilter(cv::KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt);
  void publishPose( cv::Mat measurements);
  void updateKalmanFilter( cv::KalmanFilter &KF, cv::Mat &measurement,
                           cv::Mat &translation_estimated, cv::Mat &rotation_estimated );


  /** The rectification matrix */
  cv::Mat rectification_matrix;
  /** The projection matrix */
  cv::Mat projection_matrix;


  /// Setting and getting parameters

  void setBackProjectionPixelTolerance(double);
  double getBackProjectionPixelTolerance();

  void setNearestNeighbourPixelTolerance(double);
  double getNearestNeighbourPixelTolerance();

  void setCertaintyThreshold(double);
  double getCertaintyThreshold();

  void setValidCorrespondenceThreshold(double);
  double getValidCorrespondenceThreshold();

  void setHistogramThreshold(double);
  double getHistogramThreshold();


  cv::Mat get_A_matrix() const { return camera_matrix; }
  cv::Mat get_R_matrix() const { return R_matrix; } // rotational matrix
  cv::Mat get_t_matrix() const { return t_matrix; } // translation matrix
  cv::Mat get_P_matrix() const { return P_matrix; } // rotation-translation matrix


  void fillMeasurements(cv::Mat &measurements, const cv::Mat &translation_measured, const cv::Mat &rotation_measured);
  cv::Mat rot2euler(const cv::Mat & rotationMatrix);
  cv::Mat euler2rot(const cv::Mat & euler);


  void setMarkerPositions(List3DPoints);

  // static ?
  void findLeds(const cv::Mat &image, cv::Rect ROI, const int &threshold_value, const double &gaussian_sigma,
                const double &min_blob_area, const double &max_blob_area,
                const double &max_width_height_distortion, const double &max_circular_distortion,
                std::vector<cv::Point2f> &distorted_detection_centers,
                const cv::Mat &camera_matrix_K, const std::vector<double> &camera_distortion_coeffs);

  // void setImagePoints(List2DPoints points);


  void setListPoints2d (std::vector<cv::Point2f> distorted_detection_centers);

private:

  bool readConfigs(std::string config);
  void cameraInfoCallback(const sensor_msgs::CameraInfo::ConstPtr& msg);
  void imageCallback(const sensor_msgs::Image::ConstPtr& image_msg);
  //void setListPoints2d (std::vector<cv::Point2f> distorted_detection_centers);
  void publishImageWithDetectedLEDs(const sensor_msgs::Image::ConstPtr &image_msg);


  bool findBlobCenters(const cv::Mat &img, std::vector<cv::Point2f> &centers);
  bool point_comparator_distance(const cv::Point2f &a, const cv::Point2f &b);
  bool point_comparator_bottom_left(const cv::Point2f &a, const cv::Point2f &b);
  bool point_comparator_bottom_right(const cv::Point2f &a, const cv::Point2f &b);
  bool point_comparator_top_right(const cv::Point2f &a, const cv::Point2f &b);


  void getEulerAngles(cv::Mat &rotCamerMatrix,cv::Vec3d &eulerAngles);

  void estimatePosePnp();

  void predictKalmanFilter( cv::KalmanFilter &KF, cv::Mat &translation_estimated, cv::Mat &rotation_estimated );


  void init();
  void readParameters();


  ros::NodeHandle n;
  ros::Subscriber imageSub;
  ros::Subscriber cameraInfoSub;

  ros::Publisher numPoints;
  ros::Publisher imageWLed;
  ros::Publisher posePub;

  nav_msgs::Odometry odometryMsg;
  ros::Publisher posePublisher;
  std::string poseTopicName;



  //OpenCV
  cv::Mat measurements;
  bool startFiltering;
  cv::Mat R, R_filtered;
  cv::Mat T, T_filtered;
  int numFrames, numGoodPoses, numBadPoses;
  cv::Rect roi,enlargedRoi;
  cv::Mat mask;
  //  ros::Timer timer;
  cv::Mat rvec,tvec,rvec_temp,tvec_temp;

  cv::Mat image;


  //image_transport::Publisher imageWLed; //!< The ROS image publisher that publishes the visualisation image

  bool have_camera_info; //!< The boolean variable that indicates whether the camera calibration parameters have been obtained from the camera
  sensor_msgs::CameraInfo cam_info; //!< Variable to store the camera calibration parameters
  cv::Mat camera_matrix; //!< Variable to store the camera matrix as an OpenCV matrix
  std::vector<double> camera_distortion_coeffs; //!< Variable to store the camera distortion parameters

  cv::Mat R_matrix;   // rotation matrix
  cv::Mat t_matrix;   // translation matrix
  cv::Mat P_matrix;   // rotation-translation matrix

  List3DPoints object_points; //!< Stores the positions of the makers/LEDs on the object being tracked in the object-fixed coordinate frame using homogeneous coordinates. It is a vector of 4D points.
  // List2DPoints image_points; //!< Stores the positions of the detected marker points found in the image. It is a vector of 2D points.
  List2DPoints predicted_pixel_positions; //!< Stores the predicted pixel positions of the markers in the image. \see predictMarkerPositionsInImage


  std::vector<cv::Point3f> list_points3f;
  std::vector<cv::Point2f> list_points2f;

  //std::vector<cv::Point2f> list_points2f_inliers;

  double detection_threshold_value; //!< The current threshold value for the image for LED detection
  double gaussian_sigma; //!< The current standard deviation of the Gaussian that will be applied to the thresholded image for LED detection
  double min_blob_area; //!< The the minimum blob area (in pixels squared) that will be detected as a blob/LED. Areas having an area smaller than this will not be detected as LEDs.
  double max_blob_area; //!< The the maximum blob area (in pixels squared) that will be detected as a blob/LED. Areas having an area larger than this will not be detected as LEDs.
  double max_width_height_distortion; //!< This is a parameter related to the circular distortion of the detected blobs. It is the maximum allowable distortion of a bounding box around the detected blob calculated as the ratio of the width to the height of the bounding rectangle. Ideally the ratio of the width to the height of the bounding rectangle should be 1.
  double max_circular_distortion; //!< This is a parameter related to the circular distortion of the detected blobs. It is the maximum allowable distortion of a bounding box around the detected blob, calculated as the area of the blob divided by pi times half the height or half the width of the bounding rectangle.
  double roi_border_thickness; //!< This is the thickness of the boarder (in pixels) around the predicted area of the LEDs in the image that defines the region of interest for image processing and detection of the LEDs.


  double back_projection_pixel_tolerance; //!< Stores the pixel tolerance that determines whether a back projection is valid or not. \see checkCorrespondences, initialise
  double nearest_neighbour_pixel_tolerance; //!< Stores the pixel tolerance that determines the correspondences between the LEDs and the detections in the image when predicting the position of the LEDs in the image. \see findCorrespondences
  double certainty_threshold; //!< Stores the ratio of how many of the back-projected points must be within the #back_projection_pixel_tolerance_ for a correspondence between the LEDs and the detections to be correct.
  double valid_correspondence_threshold; //!< Stores the ratio of how many correspondences must be considered to be correct for the total correspondences to be considered correct. \see checkCorrespondences, initialise
  double histogram_threshold; //!< Stores the minimum numbers of entries in the initialisation histogram before an entry could be used to determine a correspondence between the LEDs and the image detections. \see correspondencesFromHistogram

  std::vector<cv::Point2f> distorted_detection_centers;

  cv::Rect region_of_interest; //!< OpenCV rectangle that defines the region of interest to be processd to find the LEDs in the image
  static const unsigned min_num_leds_detected = 4; //!< Minimum number of LEDs that need to be detected for a pose to be calculated




};

#endif /* PNPOPENCV_H_ */
