#include "dronePnpPoseEstimation.h"


std::ofstream output("/home/zorana/Desktop/brojTacaka.txt");


PnpOpencv::PnpOpencv(): have_camera_info(false)
{
  // Initialize subscribers
  imageSub = n.subscribe("/camera/image_raw", 1, &PnpOpencv::imageCallback, this);
  cameraInfoSub = n.subscribe("/camera/camera_info", 1, &PnpOpencv::cameraInfoCallback, this);
  numPoints = n.advertise<std_msgs::Int8>("number_of_detected_points",1);
  imageWLed = n.advertise<sensor_msgs::Image>("image_with_detected_leds",1);
  posePub = n.advertise<geometry_msgs::PoseStamped>("estimated_pose",1);
  posePublisher = n.advertise<nav_msgs::Odometry>("pose_estimator/pose",1);


  init();
  readParameters();


  //  // Create the marker positions from the test points
  //  //List4DPoints positions_of_markers_on_object;
  //  List3DPoints positions_of_markers_on_object;

  //  // Read in the marker positions from the YAML parameter file
  //  XmlRpc::XmlRpcValue points_list;
  //  if (!n.getParam("marker_positions", points_list))
  //    {
  //      ROS_ERROR_STREAM(
  //            "%s: No reference file containing the marker positions, or the file is improperly formatted. Use the 'marker_positions_file' parameter in the launch file."
  //            << ros::this_node::getName().c_str());
  //      ros::shutdown();
  //    }
  //  else
  //    {

  //      positions_of_markers_on_object.resize(points_list.size());
  //      for (int i = 0; i < points_list.size(); i++)
  //        {
  //          Eigen::Matrix<double, 3, 1> temp_point;
  //          temp_point(0) = points_list[i]["x"];
  //          temp_point(1) = points_list[i]["y"];
  //          temp_point(2) = points_list[i]["z"];
  //          //temp_point(3) = 1;
  //          positions_of_markers_on_object(i) = temp_point;

  //          //list_points3f.push_back(cv::Point3f((float)temp_point(0),(float)temp_point(1),(float)temp_point(2)));

  //          double b1 = temp_point(0);
  //          double b2 = temp_point(1);
  //          double b3 = temp_point(2);
  //        }


  list_points3f.resize(4);
//  list_points3f[0] = cv::Point3f(-0.024, -0.024, 0.037);
//  list_points3f[1] = cv::Point3f(0.096, -0.048, 0.017);
//  list_points3f[2] = cv::Point3f(0.072, 0.072, 0.131);
//  list_points3f[3] = cv::Point3f(-0.048, 0.048, 0.065);

  list_points3f[3] = cv::Point3f(0.0, 0.0, 0.037);
  list_points3f[1] = cv::Point3f(0.096, -0.048, 0.017);
  list_points3f[2] = cv::Point3f(0.072, 0.072, 0.131);
  list_points3f[0] = cv::Point3f(-0.072, 0.048, 0.065);




  // m
  //      list_points3f.push_back(cv::Point3f(-0.048, 0.048, 0.065));
  //      list_points3f.push_back(cv::Point3f(-0.024, -0.024, 0.037));
  //      list_points3f.push_back(cv::Point3f(0.096, -0.048, 0.017));
  //      list_points3f.push_back(cv::Point3f(0.072, 0.072, 0.131));

  //      /// cm
  //      list_points3f.push_back(cv::Point3f(-4.8, 4.8, 6.5));
  //      list_points3f.push_back(cv::Point3f(-2.4, -2.4, 3.7));
  //      list_points3f.push_back(cv::Point3f(9.6, -4.8, 1.7));
  //      list_points3f.push_back(cv::Point3f(7.2, 7.2, 13.1));

  std::cout << std::endl << "list_points3f  x1 = " << list_points3f[0].x << " y1 = "<<list_points3f[0].y << " z1 = " << list_points3f[0].z << std::endl;
  std::cout << std::endl << "list_points3f  x2 = " << list_points3f[1].x << " y2 = "<<list_points3f[1].y << " z2 = " << list_points3f[1].z << std::endl;

  //}
  //setMarkerPositions(positions_of_markers_on_object);
  //ROS_INFO_STREAM("The number of markers on the object are: %d" << (int )positions_of_markers_on_object.size());
}



PnpOpencv::~PnpOpencv()
{

}

void PnpOpencv::init()
{

  back_projection_pixel_tolerance = 3;
  nearest_neighbour_pixel_tolerance = 5;
  certainty_threshold = 0.75;
  valid_correspondence_threshold = 0.7;

  nStates = 18;             // the number of states
  nMeasurements = 6;        // the number of measured states
  nInputs = 0;              // the number of control actions
  dt = 1/FRAMES_PER_SECOND; // time between measurements (1/FPS)

  measurements = cv::Mat(nMeasurements, 1, CV_64F);
  measurements.setTo(cv::Scalar(0));
  R = cv::Mat::zeros(3, 3, CV_64FC1);
  T = cv::Mat::zeros(3, 1, CV_64FC1);
  R_filtered = cv::Mat::zeros(3, 3, CV_64FC1);
  T_filtered = cv::Mat::zeros(3, 1, CV_64FC1);
  tvec = cv::Mat::zeros(3, 1, CV_64FC1);
  rvec = cv::Mat::zeros(3, 1, CV_64FC1);
  tvec_temp = cv::Mat::zeros(3, 1, CV_64FC1);
  rvec_temp = cv::Mat::zeros(3, 1, CV_64FC1);
  roi=cv::Rect();
  enlargedRoi=cv::Rect();
  mask=cv::Mat();
  numFrames=numGoodPoses=numBadPoses=0;
  startFiltering=false;

  initKalmanFilter(KF, nStates, nMeasurements, nInputs, dt);

}

void PnpOpencv::readParameters()
{

  if(!ros::param::get("~threshold_value", detection_threshold_value))
    {
      detection_threshold_value = 140;
    }
  std::cout<<" detection_threshold_value = " << detection_threshold_value;

  if(!ros::param::get("~gaussian_sigma", gaussian_sigma))
    {
      gaussian_sigma = 0.6;
    }
  std::cout<<" gaussian_sigma = " << gaussian_sigma;


  if(!ros::param::get("~min_blob_area", min_blob_area))
    {
      min_blob_area = 10;
    }
  std::cout<<" min_blob_area = " << min_blob_area;


  if(!ros::param::get("~max_blob_area", max_blob_area))
    {
      max_blob_area = 200;
    }
  std::cout<<" max_blob_area = " << max_blob_area;

  if(!ros::param::get("~max_width_height_distortion", max_width_height_distortion))
    {
      max_width_height_distortion = 0.5;
    }
  std::cout<<" max_width_height_distortion = " << max_width_height_distortion;

  if(!ros::param::get("~max_circular_distortion", max_circular_distortion))
    {
      max_blob_area = 0.5;
    }
  std::cout<<" max_circular_distortion = " << max_circular_distortion;

  if(!ros::param::get("~back_projection_pixel_tolerance", back_projection_pixel_tolerance))
    {
      back_projection_pixel_tolerance = 5;
    }
  std::cout<<" back_projection_pixel_tolerance = " << back_projection_pixel_tolerance;

  if(!ros::param::get("~nearest_neighbour_pixel_tolerance", nearest_neighbour_pixel_tolerance))
    {
      nearest_neighbour_pixel_tolerance = 7;
    }
  std::cout<<" nearest_neighbour_pixel_tolerance = " << nearest_neighbour_pixel_tolerance;

  if(!ros::param::get("~certainty_threshold", certainty_threshold))
    {
      certainty_threshold = 0.75;
    }
  std::cout<<" certainty_threshold = " << certainty_threshold;

  if(!ros::param::get("~valid_correspondence_threshold", valid_correspondence_threshold))
    {
      valid_correspondence_threshold = 0.7;
    }
  std::cout<<" valid_correspondence_threshold = " << valid_correspondence_threshold;

  if(!ros::param::get("~roi_border_thickness", roi_border_thickness))
    {
      roi_border_thickness = 20;
    }
  std::cout<<" roi_border_thickness = " << roi_border_thickness;

  if(!ros::param::get("~pose_topic_name", poseTopicName))
    {
      poseTopicName = "pose_estimator/pose";
    }
  std::cout<<" pose_topic_name" << poseTopicName;



}

bool PnpOpencv::readConfigs(std::string config)
{


}

void PnpOpencv::imageCallback(const sensor_msgs::Image::ConstPtr &image_msg)
{
  try
  {

    // Check whether already received the camera calibration data
    if (!have_camera_info)
      {
        ROS_WARN_STREAM("No camera info yet...");
        return;
      }


    // Import the image from ROS message to OpenCV mat
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(image_msg, sensor_msgs::image_encodings::MONO8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    image = cv_ptr->image;

    // Get time at which the image was taken. This time is used to stamp the estimated pose and also calculate the position of where to search for the makers in the image
    double time_to_predict = image_msg->header.stamp.toSec();


    region_of_interest = cv::Rect(0, 0, image.cols, image.rows);

    // Do detection of LEDs in image
    //    findLeds(image, region_of_interest, detection_threshold_value, gaussian_sigma, min_blob_area,
    //             max_blob_area, max_width_height_distortion, max_circular_distortion,
    //             distorted_detection_centers,
    //             camera_matrix, camera_distortion_coeffs);



    cv::Mat img_thr;
    cv::threshold(image, img_thr, detection_threshold_value, 255, CV_THRESH_BINARY );


    //std::vector<cv::Point2f> centers;
    findBlobCenters(img_thr,distorted_detection_centers);


    for(int i=0; i<distorted_detection_centers.size(); i++)
      {
        //cv::circle(image,distorted_detection_centers[i],10,cv::Scalar(0));
        std::stringstream featureNumber;
        featureNumber<<i;
        cv::putText(image, featureNumber.str(),
                    cv::Point(distorted_detection_centers[i].x-10,distorted_detection_centers[i].y-10),
                    CV_FONT_HERSHEY_COMPLEX, 0.8, cv::Scalar(255,0,0));
      }

    //    //! renders text string in the image
    //    CV_EXPORTS_W void putText( Mat& img, const string& text, Point org,
    //                             int fontFace, double fontScale, Scalar color,
    //                             int thickness=1, int lineType=8,
    //                             bool bottomLeftOrigin=false );


    // cv::circle(visualized_image, distorted_detection_centers[i], 10, CV_RGB(255, 0, 0), 2);


    if (distorted_detection_centers.size() > 0)
      {
        setListPoints2d(distorted_detection_centers);
      }



    std_msgs::Int8 number_of_detected_leds;
    number_of_detected_leds.data = distorted_detection_centers.size();
    numPoints.publish(number_of_detected_leds);


    estimatePosePnp();


    //    std::vector<cv::Point2f> inlier_points;
    //    for(int i=0;i<inliers.rows;i++)
    //      inlier_points.push_back(distorted_detection_centers[inliers.at<int>(i)]);

    //    for(int i=0;i<inlier_points.size();i++)
    //      cv::circle(image,inlier_points[i],5,cv::Scalar(0));


    cv::Mat R_transformed  = R.t();
    cv::Mat T_transformed  =-R.t()*T;
    cv::Mat R_filtered_transformed = R_filtered.t();
    cv::Mat T_filtered_transformed = -R_filtered.t()*T_filtered;

    odometryMsg.header.stamp=ros::Time::now();
    odometryMsg.header.frame_id="world";
    odometryMsg.pose.pose.position.x = T_filtered_transformed.at<double>(0);//*100;
    odometryMsg.pose.pose.position.y = T_filtered_transformed.at<double>(1);//*100;
    odometryMsg.pose.pose.position.z = T_filtered_transformed.at<double>(2);//*100;

    tf2::Matrix3x3 R_tf,R_filtered_tf;

    R_tf.setValue(R_transformed.at<double>(0,0),R_transformed.at<double>(0,1),R_transformed.at<double>(0,2),
                  R_transformed.at<double>(1,0),R_transformed.at<double>(1,1),R_transformed.at<double>(1,2),
                  R_transformed.at<double>(2,0),R_transformed.at<double>(2,1),R_transformed.at<double>(2,2));

    R_filtered_tf.setValue(R_filtered_transformed.at<double>(0,0),R_filtered_transformed.at<double>(0,1),R_filtered_transformed.at<double>(0,2),
                           R_filtered_transformed.at<double>(1,0),R_filtered_transformed.at<double>(1,1),R_filtered_transformed.at<double>(1,2),
                           R_filtered_transformed.at<double>(2,0),R_filtered_transformed.at<double>(2,1),R_filtered_transformed.at<double>(2,2));

    tf2Scalar roll, pitch, yaw;
    //R_tf.getRPY(roll, pitch, yaw);
    R_filtered_tf.getRPY(roll, pitch, yaw);

    tf2::Quaternion quat(yaw,pitch,roll);
    odometryMsg.pose.pose.orientation.x = quat.x();
    odometryMsg.pose.pose.orientation.y = quat.y();
    odometryMsg.pose.pose.orientation.z = quat.z();
    odometryMsg.pose.pose.orientation.w = quat.w();

//      odometryMsg.pose.pose.orientation.x = roll;// * 180 / CV_PI;
//      odometryMsg.pose.pose.orientation.y = pitch;// * 180 / CV_PI;
//      odometryMsg.pose.pose.orientation.z = yaw;// * 180 / CV_PI;


    //odometryMsg.pose.covariance;
    cv::Mat covariance = (cv::Mat_<double>(1,6) << KF.errorCovPost.at<double>(0,0), KF.errorCovPost.at<double>(1,1), KF.errorCovPost.at<double>(2,2), KF.errorCovPost.at<double>(9,9), KF.errorCovPost.at<double>(10,10), KF.errorCovPost.at<double>(11,11));

    for(int i=0;i<6;i++)
      odometryMsg.pose.covariance[i*6+i]=covariance.at<double>(i);


    if(covariance.at<double>(0)<MAX_COVARIANCE)
      numGoodPoses++;
    else
      numBadPoses++;

    if(numBadPoses>=MAX_BAD_POSES)
      {
        init();
        odometryMsg.pose.pose.position.x=0.0;
        odometryMsg.pose.pose.position.y=0.0;
        odometryMsg.pose.pose.position.z=0.0;
        posePublisher.publish(odometryMsg);
      }

    if(numGoodPoses>=MIN_GOOD_POSES)
      {
        // cv::rectangle(image,roi,cv::Scalar(255));
        cv::rectangle(image,enlargedRoi,cv::Scalar(255,255,0));

        posePublisher.publish(odometryMsg);

        if(!T_filtered_transformed.empty())
          {
            std::stringstream st;
            st<<"Estimated position: "<<T_filtered_transformed;
            cv::putText(image, st.str(), cv::Point(10,100), CV_FONT_HERSHEY_COMPLEX, 0.8, cv::Scalar(255));
          }

      }
    else
      {
        odometryMsg.pose.pose.position.x=0.0;
        odometryMsg.pose.pose.position.y=0.0;
        odometryMsg.pose.pose.position.z=0.0;
        posePublisher.publish(odometryMsg);
      }

    publishImageWithDetectedLEDs(image_msg);

    geometry_msgs::PoseStamped poseStampedMsg;


    poseStampedMsg.header.stamp = ros::Time::now();
    poseStampedMsg.header.frame_id="world";

    poseStampedMsg.pose.position.x = T.at<double>(0);//*100;
    poseStampedMsg.pose.position.y = T.at<double>(1);//*100;
    poseStampedMsg.pose.position.z = T.at<double>(2);//*100;

    //cv::Vec3d eulerAngles;

    //getEulerAngles(R,eulerAngles)
    //yaw   = eulerAngles[1];
    // pitch = eulerAngles[0];
    // roll  = eulerAngles[2];
    //    poseStampedMsg.pose.orientation.x = eulerAngles[2];
    //    poseStampedMsg.pose.orientation.y = eulerAngles[0];
    //    poseStampedMsg.pose.orientation.z = eulerAngles[1];

    poseStampedMsg.pose.orientation.x = roll * 180 / CV_PI;
    poseStampedMsg.pose.orientation.y = pitch * 180 / CV_PI;
    poseStampedMsg.pose.orientation.z = yaw * 180 / CV_PI;


    posePub.publish(poseStampedMsg);


  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

}

void PnpOpencv::getEulerAngles(cv::Mat &rotCamerMatrix,cv::Vec3d &eulerAngles)
{

  cv::Mat cameraMatrix,rotMatrix,transVect,rotMatrixX,rotMatrixY,rotMatrixZ;
  double* _r = rotCamerMatrix.ptr<double>();
  double projMatrix[12] = {_r[0],_r[1],_r[2],0,
                           _r[3],_r[4],_r[5],0,
                           _r[6],_r[7],_r[8],0};

  cv::decomposeProjectionMatrix( cv::Mat(3,4,CV_64FC1,projMatrix),
                                 cameraMatrix,
                                 rotMatrix,
                                 transVect,
                                 rotMatrixX,
                                 rotMatrixY,
                                 rotMatrixZ,
                                 eulerAngles);

}


void PnpOpencv::estimatePosePnp()
{

  // PnP parameters
  int iterationsCount = 500;        // number of Ransac iterations.
  float reprojectionError = 10.0;    // maximum allowed distance to consider it an inlier.
  float confidence = 0.5;          // ransac successful confidence.


  //If not enough points, predict
  if (list_points2f.size() < min_num_leds_detected)
    {
      if (startFiltering)
        {
          predictKalmanFilter(KF, tvec_temp, rvec_temp);
          T_filtered = tvec_temp;
          R_filtered = euler2rot(rvec_temp);
        }
      return;
    }


  tvec=T;
  cv::Rodrigues(R,rvec);

  bool useExtrinsicGuess = false;   // if true the function uses the provided rvec and tvec values as
  // initial approximations of the rotation and translation vectors

  if(startFiltering)
    useExtrinsicGuess = true;

  int minInliersCount = 4;
  //estimatePose(list_points3d, centers, CV_P3P, inliers, NUM_ITERATIONS, MIN_REPROJECTION_ERROR + numFrames*REPROJECTION_ERROR_INCREASE_RATIO, 4);//CV_P3P 500 10.0 4 OK!
  cv::Mat inliers;


  //cv::Mat K(3 ,3, CV_64F, cameraInfoMsg.K.c_array());
  //  cv::solvePnPRansac( list_points3f, list_points2f, camera_matrix, camera_distortion_coeffs,
  //                      rvec, tvec, useExtrinsicGuess, 100, 10.0,
  //                      100, inliers, CV_P3P );

  //  //! computes the camera pose from a few 3D points and the corresponding projections. The outliers are possible.
  //  CV_EXPORTS_W void solvePnPRansac( InputArray objectPoints,
  //                                    InputArray imagePoints,
  //                                    InputArray cameraMatrix,
  //                                    InputArray distCoeffs,
  //                                    OutputArray rvec,
  //                                    OutputArray tvec,
  //                                    bool useExtrinsicGuess = false,
  //                                    int iterationsCount = 100,
  //                                    float reprojectionError = 8.0,
  //                                    int minInliersCount = 100,
  //                                    OutputArray inliers = noArray(),
  //                                    int flags = ITERATIVE);


  // RANSAC parameters
  int pnpMethod = cv::P3P;
  //SOLVEPNP_ITERATIVE;



  //  cv::solvePnPRansac(list_points3f, list_points2f, camera_matrix, camera_distortion_coeffs,
  //                     rvec, tvec, useExtrinsicGuess, 700, reprojectionError=200.0, 4, inliers, CV_P3P);

  if (list_points2f.size() == 4)
    //    cv::solvePnP(list_points3f, list_points2f, camera_matrix, camera_distortion_coeffs,
    //                 rvec, tvec, useExtrinsicGuess, CV_P3P);
    cv::solvePnPRansac( list_points3f, list_points2f, camera_matrix, camera_distortion_coeffs, rvec, tvec,
                        useExtrinsicGuess, NUM_ITERATIONS, MIN_REPROJECTION_ERROR, 4,
                        inliers, pnpMethod );


  //  cv::solvePnP(list_points3f, list_points2f, camera_matrix, camera_distortion_coeffs,
  //               rvec, tvec, false, CV_P3P);


  cv::Mat R_temp;
  cv::Rodrigues(rvec,R_temp);

  //  cv::Mat T_transformed = -R_temp.t()*tvec;

  //  std::cout<< " inliners. rows = " << inliers.rows<< std::endl;
  //  std::cout << "list 3f = " << list_points3f[0] << " " << list_points3f[1] << " " << list_points3f[2] << " " << list_points3f[3] << std::endl;
  //  std::cout << "list 2f = " << list_points2f[0] << " " << list_points2f[1] << " " << list_points2f[2] << " " << list_points2f[3] << std::endl<< std::endl;



  //  if(inliers.rows==4 && abs(T_transformed.at<double>(0))<40.0 &&
  //     T_transformed.at<double>(1)>0.0 && T_transformed.at<double>(1)<300.0 &&
  //     T_transformed.at<double>(2)<50.0 && T_transformed.at<double>(2)>0.0)
  //if (inliers.rows == 4)
  if (distorted_detection_centers.size() == 4)
    {
      //      std::vector<cv::Point2f> inlier_points;
      //      for(int i=0;i<inliers.rows;i++)
      //        inlier_points.push_back(list_points2f[inliers.at<int>(i)]);
      //      roi = cv::boundingRect(inlier_points);

      //Pose is good
      T = tvec;
      cv::Rodrigues(rvec,R);
      rvec=rot2euler(R);
      fillMeasurements(measurements, tvec, rvec);
      updateKalmanFilter( KF, measurements, tvec_temp, rvec_temp );
      T_filtered = tvec_temp;
      R_filtered=euler2rot(rvec_temp);
      startFiltering=true;
      return;
    }

  if (startFiltering)
    {
      //If got enough points, but not 4 inliers
      predictKalmanFilter(KF, tvec_temp, rvec_temp);
      T_filtered = tvec_temp;
      R_filtered = euler2rot(rvec_temp);
    }

}


void PnpOpencv::predictKalmanFilter( cv::KalmanFilter &KF, cv::Mat &translation_estimated, cv::Mat &rotation_estimated )
{
  // First predict, to update the internal statePre variable
  cv::Mat prediction = KF.predict();

  // Estimated translation
  translation_estimated.at<double>(0) = prediction.at<double>(0);
  translation_estimated.at<double>(1) = prediction.at<double>(1);
  translation_estimated.at<double>(2) = prediction.at<double>(2);

  // Estimated euler angles
  rotation_estimated.at<double>(0) = prediction.at<double>(9);
  rotation_estimated.at<double>(1) = prediction.at<double>(10);
  rotation_estimated.at<double>(2) = prediction.at<double>(11);
}


void PnpOpencv::updateKalmanFilter( cv::KalmanFilter &KF, cv::Mat &measurement,
                                    cv::Mat &translation_estimated, cv::Mat &rotation_estimated )
{
  // First predict, to update the internal statePre variable
  KF.predict();

  // The "correct" phase that is going to use the predicted value and our measurement
  cv::Mat estimated = KF.correct(measurement);

  // Estimated translation
  translation_estimated.at<double>(0) = estimated.at<double>(0);
  translation_estimated.at<double>(1) = estimated.at<double>(1);
  translation_estimated.at<double>(2) = estimated.at<double>(2);

  // Estimated euler angles
  rotation_estimated.at<double>(0) = estimated.at<double>(9);
  rotation_estimated.at<double>(1) = estimated.at<double>(10);
  rotation_estimated.at<double>(2) = estimated.at<double>(11);

}


void PnpOpencv::initKalmanFilter(cv::KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt)
{



  KF.init(nStates, nMeasurements, nInputs, CV_64F);                 // init Kalman Filter


  KF.processNoiseCov = cv::Mat::zeros(nStates,nStates,CV_64F);
  KF.processNoiseCov.at<double>(6,6) = PROCESS_NOISE;
  KF.processNoiseCov.at<double>(7,7) = PROCESS_NOISE;
  KF.processNoiseCov.at<double>(8,8) = PROCESS_NOISE;
  KF.processNoiseCov.at<double>(15,15) = PROCESS_NOISE;
  KF.processNoiseCov.at<double>(16,16) = PROCESS_NOISE;
  KF.processNoiseCov.at<double>(17,17) = PROCESS_NOISE;


  cv::setIdentity(KF.measurementNoiseCov, cv::Scalar::all(MEASUREMENT_NOISE)); // set measurement noise
  cv::setIdentity(KF.errorCovPost, cv::Scalar::all(1));             // error covariance


  //  cv::setIdentity(KF.processNoiseCov, cv::Scalar::all(1e-5));       // set process noise
  //  cv::setIdentity(KF.measurementNoiseCov, cv::Scalar::all(1e-2));   // set measurement noise
  //  cv::setIdentity(KF.errorCovPost, cv::Scalar::all(1));             // error covariance


  /** DYNAMIC MODEL **/

  //  [1 0 0 dt  0  0 dt2   0   0 0 0 0  0  0  0   0   0   0]
  //  [0 1 0  0 dt  0   0 dt2   0 0 0 0  0  0  0   0   0   0]
  //  [0 0 1  0  0 dt   0   0 dt2 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  1  0  0  dt   0   0 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  1  0   0  dt   0 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  0  1   0   0  dt 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  0  0   1   0   0 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  0  0   0   1   0 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  0  0   0   0   1 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  0  0   0   0   0 1 0 0 dt  0  0 dt2   0   0]
  //  [0 0 0  0  0  0   0   0   0 0 1 0  0 dt  0   0 dt2   0]
  //  [0 0 0  0  0  0   0   0   0 0 0 1  0  0 dt   0   0 dt2]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  1  0  0  dt   0   0]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  0  1  0   0  dt   0]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  1   0   0  dt]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   1   0   0]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   1   0]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   0   1]


  // position
  KF.transitionMatrix.at<double>(0,3) = dt;
  KF.transitionMatrix.at<double>(1,4) = dt;
  KF.transitionMatrix.at<double>(2,5) = dt;
  KF.transitionMatrix.at<double>(3,6) = dt;
  KF.transitionMatrix.at<double>(4,7) = dt;
  KF.transitionMatrix.at<double>(5,8) = dt;
  KF.transitionMatrix.at<double>(0,6) = 0.5*pow(dt,2);
  KF.transitionMatrix.at<double>(1,7) = 0.5*pow(dt,2);
  KF.transitionMatrix.at<double>(2,8) = 0.5*pow(dt,2);

  // orientation
  KF.transitionMatrix.at<double>(9,12) = dt;
  KF.transitionMatrix.at<double>(10,13) = dt;
  KF.transitionMatrix.at<double>(11,14) = dt;
  KF.transitionMatrix.at<double>(12,15) = dt;
  KF.transitionMatrix.at<double>(13,16) = dt;
  KF.transitionMatrix.at<double>(14,17) = dt;
  KF.transitionMatrix.at<double>(9,15) = 0.5*pow(dt,2);
  KF.transitionMatrix.at<double>(10,16) = 0.5*pow(dt,2);
  KF.transitionMatrix.at<double>(11,17) = 0.5*pow(dt,2);


  /** MEASUREMENT MODEL **/

  //  [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
  //  [0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
  //  [0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
  //  [0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0]
  //  [0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0]
  //  [0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0]

  KF.measurementMatrix.at<double>(0,0) = 1;  // x
  KF.measurementMatrix.at<double>(1,1) = 1;  // y
  KF.measurementMatrix.at<double>(2,2) = 1;  // z
  KF.measurementMatrix.at<double>(3,9) = 1;  // roll
  KF.measurementMatrix.at<double>(4,10) = 1; // pitch
  KF.measurementMatrix.at<double>(5,11) = 1; // yaw

  std::cout<< "Init Kalman Filter done! "<< std::endl;

}


//// Set measurement to predict
//measurements.at<double>(0) = translation_measured.at<double>(0); // x
//measurements.at<double>(1) = translation_measured.at<double>(1); // y
//measurements.at<double>(2) = translation_measured.at<double>(2); // z
//measurements.at<double>(3) = rotation_measured.at<double>(0);      // roll
//measurements.at<double>(4) = rotation_measured.at<double>(1);      // pitch
//measurements.at<double>(5) = rotation_measured.at<double>(2);      // yaw
//}

void PnpOpencv::fillMeasurements( cv::Mat &measurements,
                                  const cv::Mat &translation_measured, const cv::Mat &rotation_measured)
{
  //  // Convert rotation matrix to euler angles
  //  cv::Mat measured_eulers(3, 1, CV_64F);
  //  measured_eulers = rot2euler(rotation_measured);

  // Set measurement to predict
  measurements.at<double>(0) = translation_measured.at<double>(0); // x
  measurements.at<double>(1) = translation_measured.at<double>(1); // y
  measurements.at<double>(2) = translation_measured.at<double>(2); // z
  measurements.at<double>(3) = rotation_measured.at<double>(0);      // roll
  measurements.at<double>(4) = rotation_measured.at<double>(1);      // pitch
  measurements.at<double>(5) = rotation_measured.at<double>(2);      // yaw
}


void PnpOpencv::cameraInfoCallback(const sensor_msgs::CameraInfo::ConstPtr &msg)
{


  if (!have_camera_info)
    {
      cam_info = *msg;

      // Calibrated camera
      camera_matrix = cv::Mat(3, 3, CV_64F);
      camera_matrix.at<double>(0, 0) = cam_info.K[0];
      camera_matrix.at<double>(0, 1) = cam_info.K[1];
      camera_matrix.at<double>(0, 2) = cam_info.K[2];
      camera_matrix.at<double>(1, 0) = cam_info.K[3];
      camera_matrix.at<double>(1, 1) = cam_info.K[4];
      camera_matrix.at<double>(1, 2) = cam_info.K[5];
      camera_matrix.at<double>(2, 0) = cam_info.K[6];
      camera_matrix.at<double>(2, 1) = cam_info.K[7];
      camera_matrix.at<double>(2, 2) = cam_info.K[8];
      camera_distortion_coeffs = cam_info.D;

      R_matrix = cv::Mat::zeros(3, 3, CV_64FC1);   // rotation matrix
      t_matrix = cv::Mat::zeros(3, 1, CV_64FC1);   // translation matrix
      P_matrix = cv::Mat::zeros(3, 4, CV_64FC1);   // rotation-translation matrix

      ROS_INFO_STREAM(" cal = "<< camera_matrix.at<double>(2,2) << " " << camera_distortion_coeffs.at(3));

      have_camera_info = true;
      ROS_INFO_STREAM("Camera calibration information obtained.");

    }

}

// LED detector
void PnpOpencv::findLeds(const cv::Mat &image, cv::Rect ROI2, const int &threshold_value, const double &gaussian_sigma,
                         const double &min_blob_area, const double &max_blob_area,
                         const double &max_width_height_distortion, const double &max_circular_distortion,
                         std::vector<cv::Point2f> &distorted_detection_centers,
                         const cv::Mat &camera_matrix_K, const std::vector<double> &camera_distortion_coeffs)
{
  cv::Rect ROI = cv::Rect(0, 0, image.cols, image.rows);
  // Threshold the image
  cv::Mat bw_image;
  //cv::threshold(image, bwImage, threshold_value, 255, cv::THRESH_BINARY);
  cv::threshold(image(ROI), bw_image, threshold_value, 255, cv::THRESH_TOZERO);

  // Gaussian blur the image
  cv::Mat gaussian_image;
  cv::Size ksize; // Gaussian kernel size. If equal to zero, then the kerenl size is computed from the sigma
  ksize.width = 0;
  ksize.height = 0;
  GaussianBlur(bw_image.clone(), gaussian_image, ksize, gaussian_sigma, gaussian_sigma, cv::BORDER_DEFAULT);

  //  cv::imshow( "Gaussian", gaussian_image );
  //  cv::waitKey(1);

  // Find all contours
  std::vector<std::vector<cv::Point> > contours;
  cv::findContours(gaussian_image.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

  unsigned numPoints = 0; // Counter for the number of detected LEDs

  // Vector for containing the detected points that will be undistorted later
  std::vector<cv::Point2f> distorted_points;

  // Identify the blobs in the image
  for (unsigned i = 0; i < contours.size(); i++)
    {
      double area = cv::contourArea(contours[i]); // Blob area
      cv::Rect rect = cv::boundingRect(contours[i]); // Bounding box
      double radius = (rect.width + rect.height) / 4; // Average radius

      cv::Moments mu;
      mu = cv::moments(contours[i], false);
      cv::Point2f mc;
      mc = cv::Point2f(mu.m10 / mu.m00, mu.m01 / mu.m00) + cv::Point2f(ROI.x, ROI.y);

      // Look for round shaped blobs of the correct size
      if (area >= min_blob_area && area <= max_blob_area
          && std::abs(1 - std::min((float)rect.width / (float)rect.height, (float)rect.height / (float)rect.width))
          <= max_width_height_distortion
          && std::abs(1 - (area / (CV_PI * std::pow(rect.width / 2, 2)))) <= max_circular_distortion
          && std::abs(1 - (area / (CV_PI * std::pow(rect.height / 2, 2)))) <= max_circular_distortion)
        {
          distorted_points.push_back(mc);
          numPoints++;
        }
    }

  output<<numPoints<<"   ";

  // These will be used for the visualization
  distorted_detection_centers = distorted_points;




  if (numPoints > 0)
    {
      setListPoints2d(distorted_detection_centers);
    }


}

void PnpOpencv::setListPoints2d(std::vector<cv::Point2f> distorted_detection_centers)
{
  list_points2f.clear();

  for (int i = 0; i < distorted_detection_centers.size(); i++)
    {
      //          cv::Point2d p;
      //          p(0) = distorted_detection_centers[i].x;
      //          p(1) = distorted_detection_centers.[i].y;

      float x = distorted_detection_centers[i].x;
      float y = distorted_detection_centers[i].y;

      list_points2f.push_back(cv::Point2f((float)x,(float)y));
    }


  //    for(unsigned int i = 0; i < good_matches.size(); ++i)
  //    {
  //      cv::Point3f point3d_model = list_points3d_model[ good_matches[i].trainIdx ];  // 3D point from model
  //      cv::Point2f point2d_scene = keypoints_scene[ good_matches[i].queryIdx ].pt; // 2D point from the scene

  //      list_points3d_model_match.push_back(point3d_model);         // add 3D point
  //      list_points2d_scene_match.push_back(point2d_scene);         // add 2D point
  //    }


  std::cout << "list_points2f " << list_points2f[0] << " " << list_points2f[1] << " " << list_points2f[2] << " " << list_points2f[3] << std::endl<< std::endl;


}

void PnpOpencv::publishPose( cv::Mat measurements)
{
  geometry_msgs::PoseStamped poseStampedMsg;

  poseStampedMsg.header.stamp = ros::Time::now();

  poseStampedMsg.pose.position.x = measurements.at<double>(0);
  poseStampedMsg.pose.position.y = measurements.at<double>(1);
  poseStampedMsg.pose.position.z = measurements.at<double>(2);

  poseStampedMsg.pose.orientation.x = measurements.at<double>(3);
  poseStampedMsg.pose.orientation.y = measurements.at<double>(4);
  poseStampedMsg.pose.orientation.z = measurements.at<double>(5);

  posePub.publish(poseStampedMsg);
}

void PnpOpencv::publishImageWithDetectedLEDs(const sensor_msgs::Image::ConstPtr &image_msg)
{

  // publish visualization image
  if (imageWLed.getNumSubscribers() > 0)
    {
      cv::Mat visualized_image = image.clone();
      cv::cvtColor(visualized_image, visualized_image, CV_GRAY2RGB);
      if (distorted_detection_centers.size() > 0)
        {
          // Draw a circle around the detected LED

          for (int i = 0; i < distorted_detection_centers.size(); i++)
            {
              cv::circle(visualized_image, distorted_detection_centers[i], 10, CV_RGB(255, 255, 0), 2);

              //              //cv::circle(image,distorted_detection_centers[i],10,cv::Scalar(0));
              //              std::stringstream featureNumber;
              //              featureNumber<<i;
              //              cv::putText(image, featureNumber.str(),
              //                          cv::Point(distorted_detection_centers[i].x-10,distorted_detection_centers[i].y-10),
              //                          CV_FONT_HERSHEY_COMPLEX, 0.8, (255, 0, 0));


            }
        }


      //      for(int i=0; i<distorted_detection_centers.size(); i++)
      //        {
      //          //cv::circle(image,distorted_detection_centers[i],10,cv::Scalar(0));
      //          std::stringstream featureNumber;
      //          featureNumber<<i;
      //          cv::putText(image, featureNumber.str(),
      //                      cv::Point(distorted_detection_centers[i].x-10,distorted_detection_centers[i].y-10),
      //                      CV_FONT_HERSHEY_COMPLEX, 0.8, cv::Scalar(255,0,0));
      //        }



      // Publish image for visualization
      cv_bridge::CvImage visualized_image_msg;
      visualized_image_msg.header = image_msg->header;
      visualized_image_msg.encoding = sensor_msgs::image_encodings::BGR8;
      visualized_image_msg.image = visualized_image;

      imageWLed.publish(visualized_image_msg);
    }
}





// Converts a given Rotation Matrix to Euler angles
cv::Mat PnpOpencv::rot2euler(const cv::Mat & rotationMatrix)
{
  cv::Mat euler(3,1,CV_64F);

  double m00 = rotationMatrix.at<double>(0,0);
  double m02 = rotationMatrix.at<double>(0,2);
  double m10 = rotationMatrix.at<double>(1,0);
  double m11 = rotationMatrix.at<double>(1,1);
  double m12 = rotationMatrix.at<double>(1,2);
  double m20 = rotationMatrix.at<double>(2,0);
  double m22 = rotationMatrix.at<double>(2,2);

  double x, y, z;

  // Assuming the angles are in radians.
  if (m10 > 0.998) { // singularity at north pole
      x = 0;
      y = CV_PI/2;
      z = atan2(m02,m22);
    }
  else if (m10 < -0.998) { // singularity at south pole
      x = 0;
      y = -CV_PI/2;
      z = atan2(m02,m22);
    }
  else
    {
      x = atan2(-m12,m11);
      y = asin(m10);
      z = atan2(-m20,m00);
    }

  euler.at<double>(0) = x;
  euler.at<double>(1) = y;
  euler.at<double>(2) = z;

  return euler;
}

// Converts a given Euler angles to Rotation Matrix
cv::Mat PnpOpencv::euler2rot(const cv::Mat & euler)
{
  cv::Mat rotationMatrix(3,3,CV_64F);

  double x = euler.at<double>(0);
  double y = euler.at<double>(1);
  double z = euler.at<double>(2);

  // Assuming the angles are in radians.
  double ch = cos(z);
  double sh = sin(z);
  double ca = cos(y);
  double sa = sin(y);
  double cb = cos(x);
  double sb = sin(x);

  double m00, m01, m02, m10, m11, m12, m20, m21, m22;

  m00 = ch * ca;
  m01 = sh*sb - ch*sa*cb;
  m02 = ch*sa*sb + sh*cb;
  m10 = sa;
  m11 = ca*cb;
  m12 = -ca*sb;
  m20 = -sh*ca;
  m21 = sh*sa*cb + ch*sb;
  m22 = -sh*sa*sb + ch*cb;

  rotationMatrix.at<double>(0,0) = m00;
  rotationMatrix.at<double>(0,1) = m01;
  rotationMatrix.at<double>(0,2) = m02;
  rotationMatrix.at<double>(1,0) = m10;
  rotationMatrix.at<double>(1,1) = m11;
  rotationMatrix.at<double>(1,2) = m12;
  rotationMatrix.at<double>(2,0) = m20;
  rotationMatrix.at<double>(2,1) = m21;
  rotationMatrix.at<double>(2,2) = m22;

  return rotationMatrix;
}



//void PnpOpencv::setImagePoints(List2DPoints points)
//{
//  image_points = points;
//  //PoseEstimator::calculateImageVectors();
//}

void PnpOpencv::setMarkerPositions(List3DPoints positions_of_markers_on_object)
{

  object_points = positions_of_markers_on_object;
  //predicted_pixel_positions.resize(object_points.size());
  // histogram_threshold = Combinations::numCombinations(object_points_.size(), 3);
}


bool PnpOpencv::findBlobCenters(const cv::Mat &img, std::vector<cv::Point2f> &centers)
{

  cv::Mat canny_output;
  std::vector<std::vector<cv::Point> > contours;
  std::vector<cv::Vec4i> hierarchy;

  int thresh = CANNY_THR;

  cv::Canny(img, canny_output, thresh, thresh*2 );

  if(roi!=cv::Rect())
    {
      if(mask.empty())
        mask = cv::Mat::zeros(img.size(),CV_8U);
      enlargedRoi = roi;
      int enlargement = ROI_RESIZE_RATIO*roi.width;
      enlargedRoi.x-=enlargement/2.0;
      enlargedRoi.y-=enlargement/2.0;
      enlargedRoi.width+=enlargement;
      enlargedRoi.height+=enlargement;
      cv::rectangle(mask,enlargedRoi,cv::Scalar(255),CV_FILLED);
      cv::bitwise_not(mask,mask);
    }

  if(!mask.empty() && enlargedRoi.width < 0.5*img.rows)
    canny_output.setTo(0,mask);

  cv::findContours( canny_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

  if (contours.empty())
    return false;

  /*
    cv::RNG rng(12345);
    cv::Mat drawing = cv::Mat::zeros( canny_output.size(), CV_8UC3 );
    for( int i = 0; i< contours.size(); i++ )
    {
         cv::Scalar color = cv::Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
         cv::drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, cv::Point() );
    }
    cv::imshow("drawing",drawing);
    */


  std::vector<cv::Moments> mu(contours.size());
  for( int i = 0; i < contours.size(); i++ )
    mu[i] = moments( contours[i], false );

  centers.clear();
  for( int i = 0; i < contours.size(); i++ )
    {
      double circularity = 4*M_PI*cv::contourArea(contours[i])/(cv::arcLength(contours[i],true)*cv::arcLength(contours[i],true));

      //std::stringstream circ;
      //circ<<circularity;
      //cv::putText(canny_output, circ.str(), cv::Point(contours[i][0].x-10,contours[i][0].y-10), CV_FONT_HERSHEY_COMPLEX, 0.8, cv::Scalar(255));

      if(mu[i].m00!=0.0 && circularity>MIN_CIRCULARITY)
        centers.push_back(cv::Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 ));
    }

  if(centers.size()>=4)
    {
      if(roi != cv::Rect())
        {
          //Remove outliers far from Roi center
          std::vector<cv::Point2f> sortedPoints;
          std::sort(centers.begin(),centers.end(), bind(&PnpOpencv::point_comparator_distance, this, _1, _2));
          sortedPoints.push_back(centers[0]);
          sortedPoints.push_back(centers[1]);
          sortedPoints.push_back(centers[2]);
          sortedPoints.push_back(centers[3]);
          centers.resize(4);
          for(int i=0;i<4;i++)
            centers[i] = sortedPoints[i];
        }

      bool fakeROI;
      if(roi == cv::Rect())
        {
          fakeROI=true;
          enlargedRoi.x=0.0;
          enlargedRoi.y=0.0;
          enlargedRoi.width = img.cols;
          enlargedRoi.height = img.rows;
        }

      std::vector<cv::Point2f> sortedPoints;
      std::sort(centers.begin(),centers.end(), bind(&PnpOpencv::point_comparator_bottom_left, this, _1, _2));
      sortedPoints.push_back(centers[0]);
      centers.erase(centers.begin());

      std::sort(centers.begin(),centers.end(), bind(&PnpOpencv::point_comparator_bottom_right, this, _1, _2));
      sortedPoints.push_back(centers[0]);
      centers.erase(centers.begin());

      std::sort(centers.begin(),centers.end(), bind(&PnpOpencv::point_comparator_top_right, this, _1, _2));
      sortedPoints.push_back(centers[0]);
      centers.erase(centers.begin());

      sortedPoints.push_back(centers[0]);

      centers.resize(4);
      for(int i=0;i<4;i++)
        centers[i] = sortedPoints[i];

      if(fakeROI)
        enlargedRoi=cv::Rect();
    }


  return true;

}




bool PnpOpencv::point_comparator_distance(const cv::Point2f &a, const cv::Point2f &b)
{
  cv::Point2f center = cv::Point2f(enlargedRoi.x+enlargedRoi.width/2.0,enlargedRoi.y+enlargedRoi.height/2.0);
  return (a.x-center.x)*(a.x-center.x) + (a.y-center.y)*(a.y-center.y) <
      (b.x-center.x)*(b.x-center.x) + (b.y-center.y)*(b.y-center.y);
}

bool PnpOpencv::point_comparator_bottom_left(const cv::Point2f &a, const cv::Point2f &b)
{
  cv::Point2f bottomLeftCorner = cv::Point2f(enlargedRoi.x,enlargedRoi.y+enlargedRoi.height);
  return (a.x-bottomLeftCorner.x)*(a.x-bottomLeftCorner.x) + (a.y-bottomLeftCorner.y)*(a.y-bottomLeftCorner.y) <
      (b.x-bottomLeftCorner.x)*(b.x-bottomLeftCorner.x) + (b.y-bottomLeftCorner.y)*(b.y-bottomLeftCorner.y);
}

bool PnpOpencv::point_comparator_bottom_right(const cv::Point2f &a, const cv::Point2f &b)
{
  cv::Point2f bottomRightCorner = cv::Point2f(enlargedRoi.x+enlargedRoi.width,enlargedRoi.y+enlargedRoi.height);
  return (a.x-bottomRightCorner.x)*(a.x-bottomRightCorner.x) + (a.y-bottomRightCorner.y)*(a.y-bottomRightCorner.y) <
      (b.x-bottomRightCorner.x)*(b.x-bottomRightCorner.x) + (b.y-bottomRightCorner.y)*(b.y-bottomRightCorner.y);
}

bool PnpOpencv::point_comparator_top_right(const cv::Point2f &a, const cv::Point2f &b)
{
  cv::Point2f topRightCorner = cv::Point2f(enlargedRoi.x+enlargedRoi.width,enlargedRoi.y);
  return (a.x-topRightCorner.x)*(a.x-topRightCorner.x) + (a.y-topRightCorner.y)*(a.y-topRightCorner.y) <
      (b.x-topRightCorner.x)*(b.x-topRightCorner.x) + (b.y-topRightCorner.y)*(b.y-topRightCorner.y);
}


void PnpOpencv::setBackProjectionPixelTolerance(double tolerance)
{
  back_projection_pixel_tolerance = tolerance;
}

double PnpOpencv::getBackProjectionPixelTolerance()
{
  return back_projection_pixel_tolerance;
}

void PnpOpencv::setNearestNeighbourPixelTolerance(double tolerance)
{
  nearest_neighbour_pixel_tolerance = tolerance;
}

double PnpOpencv::getNearestNeighbourPixelTolerance()
{
  return nearest_neighbour_pixel_tolerance;
}

void PnpOpencv::setCertaintyThreshold(double threshold)
{
  certainty_threshold = threshold;
}

double PnpOpencv::getCertaintyThreshold()
{
  return certainty_threshold;
}

void PnpOpencv::setValidCorrespondenceThreshold(double threshold)
{
  valid_correspondence_threshold = threshold;
}

double PnpOpencv::getValidCorrespondenceThreshold()
{
  return valid_correspondence_threshold;
}

void PnpOpencv::setHistogramThreshold(double threshold)
{
  histogram_threshold = threshold;
}

double PnpOpencv::getHistogramThreshold()
{
  return histogram_threshold;
}


