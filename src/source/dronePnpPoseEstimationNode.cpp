
#include <opencv2/opencv.hpp>
#include <Eigen/Dense>
#include <algorithm>
#include <iostream>

#include "dronePnpPoseEstimation.h"

using namespace std;

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "pnp_opencv");

  PnpOpencv n;

  ros::spin();

  return 0;
}
